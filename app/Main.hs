module Main (main) where

import Control.Monad

data Dual a = Dual {
    real :: a,
    inft :: a
} deriving (Show)

instance Num a => Num (Dual a) where
    (Dual a b) + (Dual c d) = Dual (a+c) (b+d)
    (Dual a b) - (Dual c d) = Dual (a-c) (b-d)
    (Dual a b) * (Dual c d) = Dual (a*c) (a*d+b*c)
    fromInteger a = Dual (fromInteger a) 0
    abs (Dual a b) = Dual (abs a) (b * signum a)
    signum (Dual a _) = Dual (signum a) 0

instance Fractional a => Fractional (Dual a) where
    recip (Dual a b) = Dual (recip a) ((-b)/(a*a))
    fromRational a = Dual (fromRational a) 0

instance Floating a => Floating (Dual a) where
    pi = Dual pi 0
    exp (Dual a b) = Dual (exp a) (b * (exp a))
    log (Dual a b) = Dual (log a) (b / a)
    sin (Dual a b) = Dual (sin a) (b * (cos a))
    cos (Dual a b) = Dual (cos a) (-b * (sin a))
    asin (Dual a b) = Dual (asin a) (b / (sqrt $ 1 - a*a))
    acos (Dual a b) = Dual (acos a) (-b / (sqrt $ 1 - a*a))
    atan (Dual a b) = Dual (atan a) (b / (1 + a*a))
    sinh (Dual a b) = Dual (sinh a) (b * cosh a)
    cosh (Dual a b) = Dual (cosh a) (b * sinh a)
    asinh (Dual a b) = Dual (asinh a) (b / (sqrt $ a*a + 1))
    acosh (Dual a b) = Dual (acosh a) (b / (sqrt $ a*a - 1))
    atanh (Dual a b) = Dual (atanh a) (b / (1 - a*a))

instance Functor Dual where
    fmap f (Dual a b) = Dual (f a) (f b)

{-- TODO probably wrong
instance Applicative Dual where
    pure a = Dual a a
    liftA2 f (Dual a b) (Dual c d) = Dual (f a c) (f b d)

instance Monad Dual where
    (Dual a _) >>= f = f a
--}

instance Enum a => Enum (Dual a) where
    toEnum a = fmap toEnum $ Dual a 0
    fromEnum (Dual a _) = fromEnum a

derivative :: Num a => (Dual a -> Dual a) -> (a -> a)
derivative f x = inft $ f $ Dual x 1

-- Requires Monad
-- nthDerivative :: Num a => Int -> (Dual a -> Dual a) -> (Dual a -> Dual a)
-- nthDerivative n = (!! n) . iterate derivative'

main :: IO ()
main = do
        putStrLn $ show $ fmap f [0..10]
        putStrLn $ show $ fmap (derivative f) [0..10]

-- Example function
f :: Num a => a -> a
f x = x^2+4*x+3